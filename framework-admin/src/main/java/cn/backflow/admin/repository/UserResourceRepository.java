package cn.backflow.admin.repository;

import cn.backflow.admin.entity.UserResource;
import cn.backflow.data.pagination.Page;
import cn.backflow.data.pagination.PageRequest;
import cn.backflow.data.repository.BaseMyBatisRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class UserResourceRepository extends BaseMyBatisRepository<UserResource, Integer> {

    public Page<UserResource> findByPageRequest(PageRequest pr) {
        return pageQuery("UserResource.paging", pr);
    }

    public int insertBatch(List<UserResource> urs) {
        return sqlSession.insert("UserResource.insertBatch", urs);
    }

    public int deleteByParameter(Object parameter) {
        return sqlSession.delete("UserResource.delete", parameter);
    }

    public List<Map<String, Object>> findByParameter(Map<String, Object> parameter) {
        return sqlSession.selectList("UserResource.findByParameter", parameter);
    }
}