package cn.backflow.admin.repository;

import cn.backflow.admin.entity.FileMapping;
import cn.backflow.data.pagination.Page;
import cn.backflow.data.pagination.PageRequest;
import cn.backflow.data.repository.BaseMyBatisRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public class FileMappingRepository extends BaseMyBatisRepository<FileMapping, Integer> {

    public Page<FileMapping> findByPageRequest(PageRequest pr) {
        return pageQuery("FileMapping.paging", pr);
    }

    public int save(List<FileMapping> mappings) {
        return sqlSession.insert("FileMapping.insertBatch", mappings);
    }

    public List<FileMapping> findByKeys(List<String> keys) {
        return sqlSession.selectList("FileMapping.findByKeys", keys);
    }

    public <T> Map<T, FileMapping> findMapByKeys(List<String> keys, String mapKey) {
        return sqlSession.selectMap("FileMapping.findByKeys", keys, mapKey);
    }

    public int deleteByKeys(String[] keys) {
        return sqlSession.delete("FileMapping.deleteByKeys", keys);
    }

    public List<String> findKeyByIds(Collection<Integer> ids) {
        return sqlSession.selectList("FileMapping.findKeyByIds", ids);
    }

    public List<FileMapping> findByIds(List<Integer> ids) {
        return sqlSession.selectList("FileMapping.findByIds", ids);
    }
}