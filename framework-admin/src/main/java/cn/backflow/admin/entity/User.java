package cn.backflow.admin.entity;

import cn.backflow.data.entity.CommonEntity;
import cn.backflow.utils.Strings;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

public class User extends CommonEntity {

    @Email
    @Size(max = 128)
    private String email;
    @Size(max = 13)
    private String phone;
    @Size(max = 32)
    @NotBlank(message = "用户名不能为空")
    private String name;
    @Size(max = 32)
    private String pass;
    private String avatar;
    private Integer roleId; // 默认普通用户, id为1
    private String roleName;
    private Integer gender;
    private Integer departmentId;
    private String departmentName;
    private Date birthday;
    private Integer state;  // 状态 1-正常，0-禁用
    @DateTimeFormat
    private Date visited;

    public User() {
    }

    // 计算获取资料完整度
    public int getProfileCompletion() {
        int value = 0;
        if (Strings.isNotBlank(email)) value += 20;
        if (Strings.isNotBlank(phone)) value += 20;
        if (Strings.isNotBlank(avatar)) value += 20;
        if (birthday != null) value += 20;
        return value;
    }

    // 是否为管理员
    public boolean isAdmin() {
        return roleId != null && roleId <= 0;
    }

    // 是否为超级管理员
    public boolean isSuperAdmin() {
        return roleId != null && roleId == -1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getPass() {
        return this.pass;
    }

    public void setPass(String value) {
        this.pass = value;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String value) {
        this.phone = value;
    }

    public Date getBirthday() {
        return this.birthday;
    }

    public void setBirthday(Date value) {
        this.birthday = value;
    }

    public Integer getState() {
        return this.state;
    }

    public void setState(Integer value) {
        this.state = value;
    }

    public Date getVisited() {
        return this.visited;
    }

    public void setVisited(Date value) {
        this.visited = value;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}