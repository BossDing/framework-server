package cn.backflow.admin.entity;

import cn.backflow.data.entity.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Dict extends BaseEntity {

    @Size(min = 2, max = 32, message = "字典编码长度应为2到32之间")
    @NotBlank(message = "字典编码不能为空")
    private String code;

    @Size(max = 256)
    private String description;

    private String key;

    private String value;

    private String comment;

    private Integer parent = 0;

    private Integer seq;

    private Byte state;

    public Dict() {
    }

    public Dict(String code, String description, String key, String value, String comment, Integer seq) {
        setDescription(description);
        setSeq(seq);
        setComment(comment);
        setValue(value);
        setCode(code);
        setKey(key);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String value) {
        this.code = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getSeq() {
        return this.seq;
    }

    public void setSeq(Integer value) {
        this.seq = value;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }
}