package cn.backflow.admin.controller;

import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.service.PermissionService;
import cn.backflow.data.pagination.PageRequest;
import cn.backflow.secure.annotation.Authorization;
import cn.backflow.utils.JsonMap;
import cn.backflow.web.BaseSpringController;
import cn.backflow.web.treeable.Treeable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("permission")
public class PermissionController extends BaseSpringController {
    private static final String DEFAULT_SORT_COLUMNS = "seq desc"; //默认多列排序,example: username desc,created asc

    private final PermissionService permissionService;

    @Autowired
    public PermissionController(PermissionService permissionService) {this.permissionService = permissionService;}

    /* 查询 */
    @RequestMapping
    @Authorization("permission.view")
    public Object query(HttpServletRequest request) {
        PageRequest pageRequest = pageRequest(request, DEFAULT_SORT_COLUMNS);
        return permissionService.findPage(pageRequest);
    }

    /* treetable 列表 */
    @RequestMapping("treetable")
    @Authorization("permission.view")
    public Object treetable() {
        Collection<Permission> list = permissionService.findAll();
        return Treeable.sort(list, 0, new ArrayList<>());
    }

    /* 获取 */
    @RequestMapping("{id}")
    @Authorization("permission.view")
    public Object byId(@PathVariable Integer id) {
        return permissionService.getById(id);
    }

    @RequestMapping("jstree")
    public List<Treeable.Treenode> jstree(
            @RequestParam(value = "pid", required = false) Integer pid,
            @RequestParam(value = "selected", required = false) Collection<Comparable> selected,
            @RequestParam(value = "unselectable", required = false) Collection<Comparable> unselectable) {
        List<Permission> permissions = permissionService.findAll(Collections.singletonMap("parentId", pid));
        return Treeable.jstree(permissions, selected, unselectable);
    }

    @RequestMapping("tree")
    public List<Treeable> tree(@RequestParam(value = "pid", required = false) Integer pid) {
        List<Permission> permissions = permissionService.findAll(Collections.singletonMap("parentId", pid));
        return Treeable.tree(permissions);
    }

    @RequestMapping(value = "seq", method = RequestMethod.PUT)
    public Object seq(
            @RequestParam(value = "id") Integer id,         // 权限ID
            @RequestParam(value = "from") Integer from,     // fromIndex
            @RequestParam(value = "to") Integer to) {       // toIndex
        int effected = permissionService.updateSeq(id, from, to);
        return new JsonMap(effected > 0);
    }

    /* 保存新增 */
    @Authorization("permission.edit")
    @RequestMapping(method = RequestMethod.POST)
    public Object create(@Valid Permission permission, BindingResult errors, HttpServletRequest request) {
        JsonMap json = JsonMap.succeed();
        if (errors.hasErrors()) {
            return filedErrors(errors, json);
        }
        Pattern pattern = Pattern.compile("[,|]");
        Matcher matcher = pattern.matcher(permission.getCode());
        if (matcher.find()) {
            return json.success(false).msg("权限编码包含非法字符: [%s]", matcher.group());
        }
        String[] children = request.getParameterValues("children");
        permissionService.saveWithChildren(permission, children);
        return json;
    }

    /* 保存更新 */
    @Authorization("permission.edit")
    @RequestMapping(method = RequestMethod.PUT)
    public Object update(@Valid Permission permission, BindingResult errors) {
        JsonMap json = JsonMap.succeed();
        if (errors.hasErrors()) {
            return filedErrors(errors, json);
        }
        permissionService.update(permission);
        return json;
    }

    /* 删除 */
    @Authorization("permission.del")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public Object delete(@PathVariable Integer id) {
        List<Permission> children = permissionService.findByParent(id, true);
        if (!children.isEmpty()) {
            return JsonMap.fail("该权限包含子权限，不能直接删除。");
        }
        permissionService.deleteById(id);
        return JsonMap.succeed();
    }
}